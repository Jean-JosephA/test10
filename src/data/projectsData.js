
import cherry from '../assets/png/cherry.png';
import lips from '../assets/png/lips.png';

export const projectsData = [
    {
        id: 1,
        projectName:'OnlyFans',
        tags: ['Service 1', 'Service 2', 'Service 3', 'Service 4', 'Service 5'],
        demo: 'https://onlyfans.com/',
        image: cherry,
    },
    {
        id: 2,
        projectName: 'Fansly',
        tags: ['Service 1', 'Service 2', 'Service 3', 'Service 4', 'Service 5'],
        demo: 'https://fansly.com/',
        image: lips,
    },
  
];
